/// Statistics primitives and some commonly used distributions.

use rand::distributions::exponential::Exp1;
use rand::distributions::{Sample, IndependentSample};
use rand::{Rand, Rng};

/// The probability distribution abstraction.
pub trait Distribution<X: Copy>: IndependentSample<X> {
    /// The survival function
    fn survival(&self, x: X) -> f64;

    /// Find the point `x` such that `survival(x) == s` and `x` is in the line from `base` towards
    /// direction defined by `dir`. Note that `survival(x)` should be non-increasing towards `dir`
    /// from any `x`.
    // TODO provide a default implementation using Newton's method?
    fn inv_survival(&self, s: f64, base: X, dir: X) -> Option<X>;

    fn sample_survival<R: Rng>(&self, rng: &mut R) -> f64 {
        self.survival(self.ind_sample(rng))
    }
}

/// Bivariate distributions having conditional hazard rate defined (at least approximately).
pub trait BiCondHazard: Distribution<(f64, f64)> {
    /// Hazard rate with respect to X, conditioned on current position (x, y) and rate of change of
    /// y with respect to x (dy/dx)
    ///
    /// Note: This may not be enough to define this for all bivariate distributions, but suffices
    /// for `BivarExp`.
    fn cond_hazard_rate_x(&self, xy: (f64, f64), dy_dx: f64) -> f64;
}

#[derive(Clone)]
pub struct Exp {
    lambda: f64,
}

impl Exp {
    pub fn new(lambda: f64) -> Exp {
        Exp { lambda: lambda }
    }
}

impl IndependentSample<f64> for Exp {
    fn ind_sample<R: Rng>(&self, rng: &mut R) -> f64 {
        let Exp1(n) = rng.gen::<Exp1>();
        1. / self.lambda * n
    }
}

impl Sample<f64> for Exp {
    fn sample<R: Rng>(&mut self, rng: &mut R) -> f64 {
        self.ind_sample(rng)
    }
}

impl Distribution<f64> for Exp {
    fn survival(&self, x: f64) -> f64 {
        if x > 0. { (-self.lambda * x).exp() } else { 1. }
    }

    fn inv_survival(&self, s: f64, _: f64, _: f64) -> Option<f64> {
        if s > 0. && s < 1. {
            Some(-s.ln() / self.lambda)
        } else {
            None
        }
    }
}

#[derive(Clone)]
pub struct BivarExp {
    dist_x: Exp,
    dist_y: Exp,
}

impl BivarExp {
    pub fn new(lambda_x: f64, lambda_y: f64) -> BivarExp {
        BivarExp {
            dist_x: Exp::new(lambda_x),
            dist_y: Exp::new(lambda_y),
        }
    }
}

impl IndependentSample<(f64, f64)> for BivarExp {
    fn ind_sample<R: Rng>(&self, rng: &mut R) -> (f64, f64) {
        (self.dist_x.ind_sample(rng), self.dist_y.ind_sample(rng))
    }
}

impl Sample<(f64, f64)> for BivarExp {
    fn sample<R: Rng>(&mut self, rng: &mut R) -> (f64, f64) {
        self.ind_sample(rng)
    }
}

impl Distribution<(f64, f64)> for BivarExp {
    fn survival(&self, (x, y): (f64, f64)) -> f64 {
        self.dist_x.survival(x) * self.dist_y.survival(y)
    }

    fn inv_survival(&self, s: f64, base: (f64, f64), dir: (f64, f64)) -> Option<(f64, f64)> {
        if (s > 0. && s < 1.) && (base.0 >= 0. && base.1 >= 0.) && (dir.0 > 0. && dir.1 >= 0.) {
            let dy_dx = dir.1 / dir.0;
            // x = t + x0 ; y = (dy_dx)*t + y0
            // s = e ^ -(lambda_x*x + lambda_y*y)
            // solving for t gives:
            let t = -(s.ln() + self.dist_x.lambda * base.0 + self.dist_y.lambda * base.1) /
                    (self.dist_x.lambda + dy_dx * self.dist_y.lambda);
            Some((t + base.0, dy_dx * t + base.1))
        } else {
            None
        }
    }
}

impl BiCondHazard for BivarExp {
    fn cond_hazard_rate_x(&self, _: (f64, f64), dy_dx: f64) -> f64 {
        self.dist_x.lambda + dy_dx * self.dist_y.lambda
    }
}

#[derive(Clone)]
pub struct Weibull {
    scale: f64,
    shape: f64,
    off: f64,
}

impl Weibull {
    pub fn new(scale: f64, shape: f64, off: f64) -> Weibull {
        Weibull {
            scale: scale,
            shape: shape,
            off: off,
        }
    }
}

impl IndependentSample<f64> for Weibull {
    fn ind_sample<R: Rng>(&self, rng: &mut R) -> f64 {
        let Exp1(n) = rng.gen::<Exp1>();
        self.off + self.scale * n.powf(self.shape.recip())
    }
}

impl Sample<f64> for Weibull {
    fn sample<R: Rng>(&mut self, rng: &mut R) -> f64 {
        self.ind_sample(rng)
    }
}

impl Distribution<f64> for Weibull {
    fn survival(&self, x: f64) -> f64 {
        if x >= self.off {
            (-(x - self.off) / self.scale).powf(self.shape).exp()
        } else {
            1.
        }
    }

    fn inv_survival(&self, s: f64, _: f64, _: f64) -> Option<f64> {
        if s > 0. && s < 1. {
            Some(self.scale * (-s.ln()).powf(self.shape.recip()) + self.off)
        } else {
            None
        }
    }
}

pub enum Coin {
    Heads,
    Tails,
}

impl Coin {
    pub fn toss<R: Rng>(head_prob: f64, rng: &mut R) -> Coin {
        assert!(head_prob >= 0. && head_prob <= 1.);
        // `f64::rand` generates a number in the half-open interval `[0,1)`
        if f64::rand(rng) < head_prob {
            Coin::Heads
        } else {
            Coin::Tails
        }
    }

    pub fn is_heads(self) -> bool {
        match self {
            Coin::Heads => true,
            Coin::Tails => false,
        }
    }
}

/// A rare event sampled from `ImportanceSampler`.
pub enum RareEvent {
    TrueEvent(TrueEvent),
    PseudoEvent(PseudoEvent),
}

pub struct TrueEvent {
    event_rate: f64,
    fail_bias_prob: f64,
}

impl TrueEvent {
    pub fn likelihood_ratio(self, hazard_rate: f64, n_active: usize) -> f64 {
        (hazard_rate/self.event_rate) / (self.fail_bias_prob / n_active as f64)
    }
}

pub struct PseudoEvent {
    event_rate: f64,
    fail_bias_prob: f64,
}

impl PseudoEvent {
    pub fn likelihood_ratio(self, total_hazard_rate: f64) -> f64 {
        (1. - total_hazard_rate/self.event_rate) / (1. - self.fail_bias_prob)
    }
}

pub struct ImportanceSampler {
    inner: Exp,
    base_prob: f64,
}

impl ImportanceSampler {
    pub fn new(event_rate: f64, speedup: f64) -> ImportanceSampler {
        ImportanceSampler {
            inner: Exp::new(event_rate * speedup),
            base_prob: 0.5,
        }
    }

    /// Sample a rare event. The first element of the tuple represents the time until the event, and
    /// the second represents the type of event which also provides a method to calculate the
    /// likelihood ratio.
    ///
    /// `system_strength` should be a quantity between `0` and `1` which represents the percentage
    /// of components that are still working in the system, so that the failure biasing probability
    /// can be adjusted accordingly.
    pub fn sample_event<R: Rng>(&self, system_strength: f64, rng: &mut R) -> (f64, RareEvent) {
        let time_off = self.inner.ind_sample(rng);
        let p_fb = self.base_prob * system_strength;
        (time_off, if Coin::toss(p_fb, rng).is_heads() {
            RareEvent::TrueEvent(TrueEvent {
                event_rate: self.inner.lambda,
                fail_bias_prob: p_fb,
            })
        } else {
            RareEvent::PseudoEvent(PseudoEvent {
                event_rate: self.inner.lambda,
                fail_bias_prob: p_fb,
            })
        })
    }
}
