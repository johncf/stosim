/// Specifies the interface that must be exposed by a type which encodes a redundancy technique.

use std::collections::BTreeMap;

/// Represents the load distribution among the disks.
pub enum LoadDist {
    UniformSplit(f64),
    CustomSplit(BTreeMap<usize, f64>)
}

impl LoadDist {
    /// Get the load experienced by the disk at `index`.
    pub fn get(&self, index: usize) -> f64 {
        match self {
            &LoadDist::UniformSplit(load) => load,
            &LoadDist::CustomSplit(ref map) => map[&index],
        }
    }
}

/// The storage logic abstraction.
pub trait StorageLogic: Sized {
    type Error: ::std::fmt::Debug;

    /// Return whether the system is in a recoverable state or not.
    fn is_recoverable(&self) -> bool;

    /// Returns `Ok(())` if an additional failure was possible. Otherwise, if an invalid `slot_id`
    /// was passed or if the system was already in an unrecoverable state, `Err(_)` is returned.
    fn failed(&mut self, slot_id: usize) -> Result<(), Self::Error>;

    /// Returns `Ok(())` if repair is possible. Otherwise, if an invalid `slot_id` was passed or
    /// if the system was already in an unrecoverable state, `Err(_)` is returned.
    fn repaired(&mut self, slot_id: usize) -> Result<(), Self::Error>;

    /// Return the load distribution given the total external load of the system, and the rate at
    /// which each failed disk is repaired.
    fn load_distribution(&self, system_load: f64, repair_rate: f64) -> LoadDist;

    /// Clone the configuration and reset the system to the "shiny new" state.
    fn reset_cloned(&self) -> Self;

    /// Return the total number of disks in a fully healthy system.
    fn num_slots(&self) -> usize;

    /// Return the number of failed disks in the system.
    fn num_fails(&self) -> usize;

    /// Return the number of active disks in the system.
    fn num_active(&self) -> usize {
        self.num_slots() - self.num_fails()
    }
}
