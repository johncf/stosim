/// Specifies the interface that must be exposed by a storage system state machine.

use rand::Rng;
use std::cmp;
use std::fmt;
use std::ops;

#[derive(Clone, Copy, PartialOrd, PartialEq)]
/// Represents the simulation time during a run.
///
/// Note: Using `from_raw` or `into_raw` methods _during_ a simulation run is probably wrong.
pub struct SimTime(f64);

impl SimTime { // {{{
    pub fn start() -> SimTime {
        SimTime(0.)
    }

    /// Create `SimTime` from `f64`.
    ///
    /// **Panics** if `time < 0.` since absolute time cannot be negative.
    pub fn from_raw(time: f64) -> SimTime {
        assert!(time >= 0.);
        SimTime(time)
    }

    pub fn into_raw(self) -> f64 {
        self.0
    }
}

impl fmt::Display for SimTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Eq for SimTime {}

impl Ord for SimTime {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

/// Adding two absolute times does not make sense. Here, `f64` represents the relative difference.
///
/// **Panics** if `rhs < 0.` since time cannot be turned back.
impl ops::Add<f64> for SimTime {
    type Output = Self;
    fn add(self, rhs: f64) -> Self {
        assert!(rhs >= 0.);
        SimTime(self.0 + rhs)
    }
}

/// Subtracting two absolute times returns their relative difference, typed `f64`.
impl ops::Sub for SimTime {
    type Output = f64;
    fn sub(self, rhs: SimTime) -> f64 {
        self.0 - rhs.0
    }
} // }}}

/// A transition in state. The first component represents the simulation time, and the second
/// component represents the likelihood ratio factor of this transition.
pub enum Transition {
    Active(SimTime, f64),
    DataLost(SimTime, f64),
    Invalid,
}

/// The storage system state machine abstraction.
pub trait StorageSystem {
    /// Clone and reset all the components and time to the "initial" state
    fn reset_cloned<R: Rng>(&self, speedup: f64, rng: &mut R) -> Self;

    /// Go to next state
    fn next_state<R: Rng>(&mut self, rng: &mut R) -> Transition;
}

pub enum RunResult {
    /// The system remains active.
    Active,
    /// The system failed with given likelihood ratio.
    DataLost(f64),
}

/// Simulate the system once for `mission_time` and return the result.
pub fn run_once<S, R>(mut sys: S, mission_time: SimTime, rng: &mut R) -> RunResult
    where S: StorageSystem,
          R: Rng {
    let mut likelihood_ratio = 1.;
    loop {
        match sys.next_state(rng) {
            Transition::Active(ts, lr) => {
                likelihood_ratio *= lr;
                if ts > mission_time { break; }
            }
            Transition::DataLost(_, lr) => {
                likelihood_ratio *= lr;
                return RunResult::DataLost(likelihood_ratio);
            }
            Transition::Invalid => panic!("Bad bad system!"),
        }
    }
    RunResult::Active
}
