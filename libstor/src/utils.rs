/// Some commonly used types and functions built on top other abstractions.

use std::cell::Cell;
use std::cmp::Ordering;
use std::collections::BinaryHeap;

use rand::thread_rng;
use rand::{Rand, Rng, XorShiftRng};
use sim::run_once;
use sim::{RunResult, SimTime, StorageSystem, Transition};
use stat::{BiCondHazard, Distribution, ImportanceSampler, RareEvent};
use logic::{LoadDist, StorageLogic};

#[derive(Clone)]
enum DiskStat {
    Active {
        age: f64,
        usage: f64,
        min_survival: f64,
    },
    Failed,
}

impl DiskStat {
    fn new_active(max_surv: f64) -> DiskStat {
        DiskStat::Active {
            age: 0.,
            usage: 0.,
            min_survival: max_surv,
        }
    }
}

pub trait FailureDist: Distribution<(f64, f64)> + BiCondHazard + Clone {}

impl<T> FailureDist for T where T: Distribution<(f64, f64)> + BiCondHazard + Clone {}

pub struct HardDisk<FD>
    where FD: FailureDist {
    fail_dist: FD,
    status: DiskStat,
}

impl<FD> HardDisk<FD> // {{{
    where FD: FailureDist {
    pub fn new<R: Rng>(fail_dist: FD, rng: &mut R) -> HardDisk<FD> {
        let max_surv = fail_dist.sample_survival(rng);
        HardDisk {
            fail_dist: fail_dist,
            status: DiskStat::new_active(max_surv),
        }
    }

    pub fn is_active(&self) -> bool {
        match self.status {
            DiskStat::Active {..} => true,
            DiskStat::Failed => false,
        }
    }

    pub fn failed(&mut self) {
        assert!(self.is_active());
        self.status = DiskStat::Failed;
    }

    pub fn repaired<R: Rng>(&mut self, rng: &mut R) {
        assert!(!self.is_active());
        self.status = DiskStat::new_active(self.fail_dist.sample_survival(rng));
    }

    /// Returns time to failure depending on `usage_rate` which is d/dt(usage)
    pub fn forecast_failure(&self, usage_rate: f64) -> Result<f64, ()> {
        if let DiskStat::Active { age, usage, min_survival } = self.status {
            let base = (age, usage);
            let dir = (1., usage_rate);
            Ok(self.fail_dist
                   .inv_survival(min_survival, base, dir)
                   .map(|(age, _)| age)
                   .expect("Unable to forecast failure") - age)
        } else {
            Err(())
        }
    }

    pub fn hazard_rate(&self, usage_rate: f64) -> Result<f64, ()> {
        if let DiskStat::Active { age, usage, .. } = self.status {
            Ok(self.fail_dist.cond_hazard_rate_x((age, usage), usage_rate))
        } else {
            Err(())
        }
    }

    pub fn fast_forward(&mut self, time_since: f64, usage_rate: f64) -> Result<(), ()> {
        if let DiskStat::Active { ref mut age, ref mut usage, min_survival, .. } = self.status {
            *age += time_since;
            *usage += time_since * usage_rate;
            let cur_survival = self.fail_dist.survival((*age, *usage));
            assert!(cur_survival >= min_survival, "Bad survival value! {} < {}", cur_survival, min_survival);
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn reset_cloned<R: Rng>(&self, rng: &mut R) -> HardDisk<FD> {
        HardDisk {
            fail_dist: self.fail_dist.clone(),
            status: DiskStat::new_active(self.fail_dist.sample_survival(rng)),
        }
    }
} // }}}

pub struct RareFailure {
    pub disk_id: usize,
    pub hazard_rate: f64,
}

pub struct DiskArray<FD>
    where FD: FailureDist {
    inner: Vec<HardDisk<FD>>,
}

impl<FD> DiskArray<FD> // {{{
    where FD: FailureDist {
    /// Create a new `DiskArray`. Note: Current time is assumed to be `0.`
    pub fn new<R: Rng>(size: usize, fail_dist: FD, rng: &mut R) -> DiskArray<FD> {
        DiskArray {
            inner: (0..size).map(|_| HardDisk::new(fail_dist.clone(), rng))
                            .collect()
        }
    }

    /// Return the index and time of next failure from the disk array. `load_dist` should be either
    /// a `UniformSplit` or a `SimpleSplit` (load values for failed disks are ignored).
    ///
    /// Note: this method does not mark the disk as failed. Use `failed` method for that.
    pub fn next_failure(&self, load_dist: &LoadDist, now: SimTime) -> (SimTime, usize) {
        let iter = self.inner.iter().enumerate();
        iter.filter_map(|(idx, disk)| disk.forecast_failure(load_dist.get(idx))
                                          .ok().map(|t| (now + t, idx)))
            .min_by_key(|&(t, _)| t)
            .unwrap()
    }

    /// Returns the index of the disk to be failed and the likelihood factor of it failing.
    ///
    /// Note: this method does not mark the disk as failed. Use `failed` method for that.
    pub fn rare_failure<R: Rng>(&self, load_dist: &LoadDist, rng: &mut R) -> RareFailure {
        let iter = self.inner.iter().enumerate();
        let active: Vec<(usize, &_)> = iter.filter(|&(_, disk)| disk.is_active()).collect();
        let idx = usize::rand(rng) % active.len();
        let (idx, disk) = active[idx];
        RareFailure {
            disk_id: idx,
            hazard_rate: disk.hazard_rate(load_dist.get(idx)).unwrap(),
        }
    }

    pub fn total_hazard_rate(&self, load_dist: &LoadDist) -> f64 {
        let iter = self.inner.iter().enumerate();
        iter.filter_map(|(idx, disk)| disk.hazard_rate(load_dist.get(idx)).ok())
            .sum()
    }

    pub fn fast_forward(&mut self, time_since: f64, load_dist: &LoadDist) {
        let iter_mut = self.inner.iter_mut().enumerate();
        for (idx, disk) in iter_mut {
            // fast-forward all active disks
            let _ = disk.fast_forward(time_since, load_dist.get(idx));
        }
    }

    /// Mark the disk as failed.
    pub fn failed(&mut self, disk_id: usize) {
        self.inner[disk_id].failed();
    }

    /// Mark the disk as active.
    pub fn repaired<R: Rng>(&mut self, disk_id: usize, rng: &mut R) {
        self.inner[disk_id].repaired(rng);
    }

    /// Clones and resets all the disks to active states with times of birth at `0.`
    pub fn reset_cloned<R: Rng>(&self, rng: &mut R) -> DiskArray<FD> {
        DiskArray {
            inner: self.inner.iter().map(|hd| hd.reset_cloned(rng)).collect()
        }
    }
} // }}}

struct PendingRepair {
    eta: SimTime,
    index: usize,
}

impl PartialEq for PendingRepair { // {{{
    fn eq(&self, other: &Self) -> bool {
        self.eta.eq(&other.eta)
    }
}

impl Eq for PendingRepair {}

impl PartialOrd for PendingRepair {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // reverse the ordering since `BinaryHeap` is a max-heap and we want a min-heap
        other.eta.partial_cmp(&self.eta)
    }
}

impl Ord for PendingRepair {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(&other).unwrap()
    }
} // }}}

pub struct RepairMan<D>
    where D: Distribution<f64> + Clone {
    inner: BinaryHeap<PendingRepair>,
    dist: D,
}

impl<D> RepairMan<D> // {{{
    where D: Distribution<f64> + Clone {
    pub fn new(repr_dist: D) -> RepairMan<D> {
        RepairMan {
            inner: BinaryHeap::new(),
            dist: repr_dist,
        }
    }

    pub fn failed<R: Rng>(&mut self, index: usize, time: SimTime, rng: &mut R) {
        self.inner.push(PendingRepair { eta: time + self.dist.ind_sample(rng), index: index });
    }

    pub fn reset_cloned(&self) -> RepairMan<D> {
        RepairMan {
            inner: BinaryHeap::new(),
            dist: self.dist.clone(),
        }
    }

    /// Get the next scheduled repair.
    ///
    /// **Panics** if there are no pending repairs.
    pub fn next_repair(&self) -> (SimTime, usize) {
        self.inner.peek().map(|p| (p.eta, p.index)).unwrap()
    }

    /// Notify of the repair. `index` is used for verification.
    ///
    /// **Panics** if there are no pending repairs, or if `index` was not the next one.
    pub fn repaired(&mut self, index: usize) {
        assert_eq!(self.inner.pop().map(|p| p.index).unwrap(), index);
    }
} // }}}

pub struct SystemPlan<SL, FD, RD>
    where SL: StorageLogic,
          FD: FailureDist,
          RD: Distribution<f64> + Clone {
    pub storage_logic: SL,
    pub fail_dist: FD,
    pub repr_dist: RD,
    pub system_load: f64,
    pub repair_load: f64,
    pub max_hazard_rate: f64, // maximum total hazard rate the system may face
}

pub struct SimpleSystem<SL, FD, RD>
    where SL: StorageLogic,
          FD: FailureDist,
          RD: Distribution<f64> + Clone {
    logical: SL,
    physical: DiskArray<FD>,
    repairman: RepairMan<RD>,
    system_load: f64,
    repair_rate: f64,
    max_hazard_rate: f64,
    imp_sampler: ImportanceSampler,
    time: SimTime,
}

impl<SL, FD, RD> SimpleSystem<SL, FD, RD> // {{{
    where SL: StorageLogic,
          FD: FailureDist,
          RD: Distribution<f64> + Clone {
    pub fn new<R: Rng>(plan: SystemPlan<SL, FD, RD>,
                       speedup: f64,
                       rng: &mut R) -> SimpleSystem<SL, FD, RD> {
        let size = plan.storage_logic.num_slots();
        SimpleSystem {
            logical: plan.storage_logic,
            physical: DiskArray::new(size, plan.fail_dist, rng),
            repairman: RepairMan::new(plan.repr_dist),
            system_load: plan.system_load,
            repair_rate: plan.repair_load,
            max_hazard_rate: plan.max_hazard_rate,
            imp_sampler: ImportanceSampler::new(plan.max_hazard_rate, speedup),
            time: SimTime::start(),
        }
    }

    fn failed<R: Rng>(&mut self, index: usize, time: SimTime, rng: &mut R) {
        self.logical.failed(index).expect("Bad logical state!");
        self.physical.failed(index);
        self.repairman.failed(index, time, rng);
    }

    fn repaired<R: Rng>(&mut self, index: usize, rng: &mut R) {
        self.logical.repaired(index).expect("Bad logical state!");
        self.physical.repaired(index, rng);
        self.repairman.repaired(index);
    }
}

impl<SL, FD, RD> StorageSystem for SimpleSystem<SL, FD, RD>
    where SL: StorageLogic,
          FD: FailureDist,
          RD: Distribution<f64> + Clone {
    fn reset_cloned<R: Rng>(&self, speedup: f64, rng: &mut R) -> SimpleSystem<SL, FD, RD> {
        SimpleSystem {
            logical: self.logical.reset_cloned(),
            physical: self.physical.reset_cloned(rng),
            repairman: self.repairman.reset_cloned(),
            system_load: self.system_load,
            repair_rate: self.repair_rate,
            max_hazard_rate: self.max_hazard_rate,
            imp_sampler: ImportanceSampler::new(self.max_hazard_rate, speedup),
            time: SimTime::start(),
        }
    }

    fn next_state<R: Rng>(&mut self, rng: &mut R) -> Transition {
        let ref load_dist = self.logical.load_distribution(self.system_load, self.repair_rate);
        let num_active = self.logical.num_active();

        let lr_factor;
        let t_new;
        if self.logical.num_fails() > 0 { // degraded mode
            let (t_rare, rare_event) = {
                let system_strength = num_active as f64 / self.logical.num_slots() as f64;
                let (t_rel, event) = self.imp_sampler.sample_event(system_strength, rng);
                (self.time + t_rel, event)
            };
            let (t_repr, i_repr) = self.repairman.next_repair();
            let (t_fail, i_fail) = self.physical.next_failure(load_dist, self.time);
            if t_repr <= t_rare && t_repr <= t_fail { // repair event next
                t_new = t_repr;
                self.physical.fast_forward(t_new - self.time, load_dist);
                self.repaired(i_repr, rng);
                lr_factor = 1.;
            } else if t_rare <= t_fail && t_rare <= t_repr { // rare event next
                t_new = t_rare;
                self.physical.fast_forward(t_new - self.time, load_dist);
                lr_factor = match rare_event {
                    RareEvent::TrueEvent(event) => {
                        let RareFailure { disk_id, hazard_rate } = self.physical.rare_failure(load_dist, rng);
                        self.failed(disk_id, t_new, rng);
                        event.likelihood_ratio(hazard_rate, num_active)
                    }
                    RareEvent::PseudoEvent(event) => {
                        event.likelihood_ratio(self.physical.total_hazard_rate(load_dist))
                    }
                };
            } else { // normal failure event next
                t_new = t_fail;
                self.failed(i_fail, t_new, rng);
                self.physical.fast_forward(t_new - self.time, load_dist);
                lr_factor = 1.;
            }
        } else { // normal mode
            let (t_fail, i_fail) = self.physical.next_failure(load_dist, self.time);
            t_new = t_fail;
            self.failed(i_fail, t_new, rng);
            self.physical.fast_forward(t_new - self.time, load_dist);
            lr_factor = 1.;
        }

        assert!(self.time <= t_new, "Attempted time travel! {} > {}", self.time, t_new);
        self.time = t_new;

        if self.logical.is_recoverable() {
            Transition::Active(self.time, lr_factor)
        } else {
            Transition::DataLost(self.time, lr_factor)
        }
    }
} // }}}

#[derive(Debug)]
/// Unreliability mean, 95% confidence error from mean, and total recover count
pub struct SimResult {
    pub unreliability_mean: f64,
    pub unreliability_error: f64,
    pub recover_fraction: f64,
}

// TODO use manual seeding for deterministic repetition for debugging?
pub fn simulate_repeat<S>(sys: &S, mission_time: SimTime, speedup: f64, repeat: usize) -> SimResult
    where S: StorageSystem {
    let ref reco_count = Cell::new(0); // replace with AtomicUsize when parallelizing
    let ref mut rng: XorShiftRng = thread_rng().gen();
    // to parallelize with rayon, use `into_par_iter` instead of `into_iter`, and
    // `reduce_with_identity` instead of `fold`.
    let (sum_lr, sum_lr2) =
        (0..repeat).into_iter()
              .map(move |_| {
                  let sys = sys.reset_cloned(speedup, rng);
                  run_once(sys, mission_time, rng)
              })
              .filter_map(move |run_res| {
                  match run_res {
                      RunResult::Active => {
                          reco_count.set(reco_count.get() + 1);
                          None
                      },
                      RunResult::DataLost(lr) => Some(lr),

                  }
              })
              .map(|lr| (lr, lr.powi(2))) // (x, x^2)
              .fold((0., 0.), |(x1, y1), (x2, y2)| (x1 + x2, y1 + y2)); // (sum(x), sum(x^2))

    let (mean, var) = (sum_lr / repeat as f64, sum_lr2 / repeat as f64);
    // for 95% conf level, 1.960 is the 0.975 quantile of normal distribution
    let error = 1.645 * (var / repeat as f64).sqrt();
    SimResult {
        unreliability_mean: mean,
        unreliability_error: error,
        recover_fraction: reco_count.get() as f64 / repeat as f64,
    }
}

pub fn estimate_speedup<S>(sys: &S, mission_time: SimTime, repeat_per_step: usize) -> Result<f64, f64>
    where S: StorageSystem {
    let (mut lo, mut hi) = (None, None);
    let (rf_lo, rf_hi) = (0.2, 0.9);
    let mut tries = 0;
    let mut speedup = 40.;
    loop {
        let res = simulate_repeat(sys, mission_time, speedup, repeat_per_step);
        let reco_frac = res.recover_fraction;
        if reco_frac < rf_lo { // too few recovered -- decrease speedup
            hi = Some(speedup);
            match lo {
                Some(l) => speedup = (l + speedup) / 2.,
                None => speedup /= 2.,
            }
        } else if reco_frac > rf_hi {
            lo = Some(speedup);
            match hi {
                Some(h) => speedup = (speedup + h) / 2.,
                None => speedup *= 2.,
            }
        } else {
            break;
        }
        tries += 1;
        if tries > 16 {
            return Err(speedup);
        }
    }
    println!("Estimated speedup: {}", speedup);
    Ok(speedup)
}

pub fn analyze<SL, FD, RD>(plan: SystemPlan<SL, FD, RD>,
                            mission_time: SimTime,
                            repeat: usize) -> SimResult
    where SL: StorageLogic,
          FD: FailureDist,
          RD: Distribution<f64> + Clone {
    let sys = SimpleSystem::new(plan, 40., &mut thread_rng());

    match estimate_speedup(&sys, mission_time, repeat/20) {
        Ok(speedup) => simulate_repeat(&sys, mission_time, speedup, repeat),
        Err(speedup) => panic!("Speedup could not be estimated. Final try: {:e}", speedup),
    }
}

#[cfg(test)]
mod tests {
    use super::{HardDisk, DiskStat};
    use stat::{BivarExp};

    #[test]
    fn test_forecast() {
        let mut hdd = HardDisk {
            fail_dist: BivarExp::new(1./100., 1./20.),
            status: DiskStat::new_active(0.1),
        };
        assert_eq!(hdd.forecast_failure(4.).unwrap().round(), 11.); // 10.96469
        assert_eq!(hdd.forecast_failure(8.).unwrap().round(), 6.); // 5.61606
        assert_eq!(hdd.forecast_failure(16.).unwrap().round(), 3.); // 2.84270
        hdd.fast_forward(5., 8.).unwrap();
        assert_eq!(hdd.forecast_failure(2.).unwrap().round(), 2.); // 2.29623
    }
}
