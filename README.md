# StoSim
A storage system simulator tool written in [Rust][]. The aim is to provide a
tool which can be used to model and simulate simple storage architectures.

As of now, it can only simulate [MDS][] schemes. The [input file][] supports
the following parameters:

    fail_dist     : The parameters of the marginal exponential distributions of
                    the bivariate failure distribution
    repr_dist     : The Weibull parameters of the repair distribution
    mds_conf      : MDS configuration: (# data disks, # repair disks)
    system_load   : The external load on the system
    repair_load   : The additional repair load on the system
    mission_time  : The mission time
    repeat        : Number of times the simulation should be repeated

[Rust]: https://github.com/rust-lang/rust/
[MDS]: https://en.wikipedia.org/wiki/Singleton_bound#MDS_codes
[input file]: ./example_input

## Compiling and running
From the project's top directory, run:
```
cargo run --release < example_input
```
