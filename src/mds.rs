use stor::logic::{StorageLogic, LoadDist};

pub struct MDS {
    data: usize,
    parity: usize,
    num_fails: usize,
}

impl MDS {
    pub fn new(data: usize, parity: usize) -> MDS {
        MDS {
            data: data,
            parity: parity,
            num_fails: 0,
        }
    }
}

impl StorageLogic for MDS {
    type Error = ();

    fn is_recoverable(&self) -> bool {
        self.num_fails <= self.parity
    }

    /// Notify a disk failure. Returns an error only if it is already in an unrecoverable state.
    ///
    /// Note: The second argument is ignored. So the correctness of the call is up to the caller.
    fn failed(&mut self, _: usize) -> Result<(), ()> {
        if !self.is_recoverable() {
            Err(())
        } else {
            self.num_fails += 1;
            Ok(())
        }
    }

    /// Notify a disk repair. Returns an error only if it is already in an unrecoverable state.
    ///
    /// Note: The second argument is ignored. So the correctness of the call is up to the caller.
    fn repaired(&mut self, _: usize) -> Result<(), ()> {
        if !self.is_recoverable() {
            Err(())
        } else {
            self.num_fails -= 1;
            Ok(())
        }
    }

    /// Assumes distributed parity, and the additional load due to repairs gets distributed
    /// uniformly.
    fn load_distribution(&self, total_load: f64, repair_rate: f64) -> LoadDist {
        // Note: in mds, multiple disks can be repaired in parallel with no additional IO from
        // active disks (just a little more computation).
        let repair_load_per_disk = if self.num_fails > 0 {
            repair_rate
        } else {
            0.
        };
        // While repairing, since parity is distributed, a portion of real data must be
        // reconstructed to serve read requests, which means `n_failures/n_total * total_load`
        // additional load would be experienced by `n_data` disks at all points in time.
        // I.e. an additional `n_data/n_active * n_failures/n_total * total_load` per active disk.
        let n_active = self.num_active() as f64;
        let n_total = self.num_slots() as f64;
        let read_reconstruction_overhead = self.data as f64 / n_active *
                                           self.num_fails as f64 / n_total *
                                           total_load;
        LoadDist::UniformSplit(total_load / n_active + repair_load_per_disk + read_reconstruction_overhead)
    }

    fn reset_cloned(&self) -> MDS {
        MDS {
            data: self.data,
            parity: self.parity,
            num_fails: 0,
        }
    }

    fn num_slots(&self) -> usize {
        self.data + self.parity
    }

    fn num_fails(&self) -> usize {
        self.num_fails
    }
}

#[cfg(test)]
mod tests {
    use super::MDS;
    use stor::logic::{LoadDist, StorageLogic};

    fn assert_load(load: LoadDist, expected_load: f64) {
        match load {
            LoadDist::UniformSplit(l) => if l != expected_load {
                panic!("Wrong load distribution: {}", l)
            },
            _ => unreachable!(),
        }
    }

    #[test]
    fn test_load_dist() {
        let mut mds_conf = MDS::new(6, 2);
        assert_load(mds_conf.load_distribution(80., 5.), 10.); // 80 / 8
        mds_conf.failed(0).unwrap();
        assert_load(mds_conf.load_distribution(70., 5.), 15. + 7.5); // 70 / 7 + 5 + 6/7*1/8*70
        mds_conf.failed(0).unwrap(); // the index does not matter
        assert_load(mds_conf.load_distribution(60., 5.), 15. + 15.); // 60 / 6 + 5 + 6/6*2/8*60
        mds_conf.repaired(0).unwrap();
        assert_load(mds_conf.load_distribution(70., 5.), 15. + 7.5);
    }
}
