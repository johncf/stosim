extern crate stor;
extern crate rand;
extern crate rustc_serialize;

mod mds;

use std::io::{self, Read};
use rustc_serialize::json;

use stor::{SimTime, analyze};
use stor::stat::{BivarExp, Weibull};
use stor::utils::SystemPlan;

use mds::MDS;

#[derive(RustcDecodable)]
struct SimParams {
    fail_dist: (f64, f64),
    repr_dist: (f64, f64, f64),
    mds_conf: (usize, usize),
    system_load: f64,
    repair_load: f64,
    mission_time: f64,
    repeat: usize,
}

fn main() {
    let mut stdin = io::stdin();
    let params: SimParams;
    let mut sys_json = String::new();
    params = match stdin.read_to_string(&mut sys_json) {
        Ok(_) => json::decode(&*sys_json).unwrap(),
        Err(e) => panic!("Fatal error: {:?}", e),
    };
    let (mds_data, mds_parity) = params.mds_conf;
    let max_io_rate = params.system_load / mds_data as f64 + params.repair_load;
    let plan = SystemPlan {
        storage_logic: MDS::new(mds_data, mds_parity),
        fail_dist: BivarExp::new(params.fail_dist.0, params.fail_dist.1),
        repr_dist: Weibull::new(params.repr_dist.0, params.repr_dist.1, params.repr_dist.2),
        system_load: params.system_load,
        repair_load: params.repair_load,
        max_hazard_rate: (mds_data + mds_parity) as f64 *
                         (params.fail_dist.0 + params.fail_dist.1 * max_io_rate), // FIXME ?
    };
    let result = analyze(plan, SimTime::from_raw(params.mission_time), params.repeat);
    println!("Fraction of recoveries: {:.3}\nUnreliability: {:.3e} \u{00b1} {:.3e}",
             result.recover_fraction,
             result.unreliability_mean,
             result.unreliability_error);
}
