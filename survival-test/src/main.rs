extern crate stor;
extern crate rand;
extern crate rustc_serialize;

use rand::thread_rng;
use rand::{Rand, Rng, XorShiftRng};
use rustc_serialize::json;
use std::io::{stdin, Read};
//use std::io::{stderr, Write}
use stor::stat::{BivarExp, Distribution};

#[derive(RustcDecodable)]
struct TestParams {
    biexp_params: (f64, f64),
    rate_range: (f64, f64), // rate of change of one variable w.r.t other
    repeat: usize, // number of samples
}

fn main() {
    let mut p_json = String::new();
    stdin().read_to_string(&mut p_json).expect("Error reading from stdin");
    let params: TestParams = json::decode(&*p_json).expect("Error decoding json");

    let biexp = BivarExp::new(params.biexp_params.0, params.biexp_params.1);
    let ref mut rng: XorShiftRng = thread_rng().gen();
    let rate_norm_factor = params.biexp_params.0 / params.biexp_params.1;
    let (min_rate_norm, max_rate_norm) = (params.rate_range.0 / rate_norm_factor,
                                          params.rate_range.1 / rate_norm_factor);
    let (min_angle, max_angle) = (min_rate_norm.atan(), max_rate_norm.atan());
    for _ in 0..params.repeat {
        let angle = f64::rand(rng) * (max_angle - min_angle) + min_angle;
        let rate = angle.tan() * rate_norm_factor;
        //writeln!(&mut stderr(), "{}", rate);
        let min_surv = biexp.sample_survival(rng);
        let fail_at = biexp.inv_survival(min_surv, (0., 0.), (1., rate)).unwrap();
        println!("{} {}", fail_at.0, fail_at.1);
    }
}
