#!/usr/bin/env python3
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from pylab import loadtxt

x = loadtxt('out2.log')

# the histogram of the data
n, bins, patches = plt.hist(x, 50, normed=1, facecolor='green', alpha=0.75)

plt.xlabel('Angle')
plt.ylabel('Probability')
#plt.axis([40, 160, 0, 0.03])
plt.grid(True)

plt.show()
