#!/bin/python3

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
from pylab import loadtxt

def plot(ax, xs, ys, plttype="plot", color='r', heat_limits=None):
    if plttype == "scatter":
        ax.scatter(xs, ys, s=5, marker=".", color=color)
    elif plttype == "heat" and heat_limits:
        bins = int(len(xs)/30)
        x_slice = heat_limits[0]/bins
        y_slice = heat_limits[1]/bins
        zv = np.zeros((bins, bins))
        for (x, y) in zip(xs, ys):
            xi = int(x/x_slice)
            yi = int(y/y_slice)
            if xi < bins and yi < bins:
                zv[yi, xi] += 1
        ax.imshow(zv, origin='lower', extent=(0, heat_limits[0], 0, heat_limits[1]), aspect=1.5/2*x_slice/y_slice)
    else:
        ax.plot(xs, ys, color=color)
    #for tl in ax.get_yticklabels():
    #    tl.set_color(color)

def plot_twin(ax, xs, ys, ylabel, plttype="plot", color='b'):
    ax2 = ax.twinx()
    plot(ax2, xs, ys, ylabel, plttype, color)

data = loadtxt('out2')
fig, ax = plt.subplots()

ax.set_xlabel("X")
ax.set_ylabel("Y")
#ax.set_xlim([0, 2e5])
#ax.set_ylim([0, 4e6])
plot(ax, data[:,0], data[:,1], plttype="heat", heat_limits=[2e5, 4e6])

fig.savefig("ploth.svg", bbox_inches="tight", dpi=96)
